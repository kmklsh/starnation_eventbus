package com.starnation.eventbus;

import android.support.annotation.NonNull;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 * @author lsh
 * @since 2015. 10. 27.
*/
public final class EventBus {

    //======================================================================
    // Constants
    //======================================================================

    private final static String KEY_FORMAT = "EventBus.key.%d";

    //======================================================================
    // Variables
    //======================================================================

    private final static EventBus INSTANCE = new EventBus();

    private final ConcurrentHashMap<String, org.greenrobot.eventbus.EventBus> mMap = new ConcurrentHashMap<>();

    private final Object mLock = new Object();

    //======================================================================
    // Constructor
    //======================================================================

    public static EventBus with() {
        return INSTANCE;
    }

    @Deprecated
    public EventBus() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void register(@NonNull Object object) {
        registerInternal(object);
    }

    public void unRegister(@NonNull Object object) {
        unRegisterInternal(object);
    }

    public void postAll(@NonNull Event event) {
        synchronized (mLock) {
            if (mMap.isEmpty()) {
                return;
            }

            for (Map.Entry<String, org.greenrobot.eventbus.EventBus> stringEventBusEntry : mMap.entrySet()) {
                org.greenrobot.eventbus.EventBus eventBus = stringEventBusEntry.getValue();
                if (eventBus != null) {
                    eventBus.post(event);
                }
            }
        }
    }

    public void postAll(@NonNull Event event, Object... exclude) {
        synchronized (mLock) {
            if (mMap.isEmpty()) {
                return;
            }

            for (Map.Entry<String, org.greenrobot.eventbus.EventBus> stringEventBusEntry : mMap.entrySet()) {
                org.greenrobot.eventbus.EventBus eventBus = stringEventBusEntry.getValue();

                boolean call = true;
                if (exclude != null){
                    for (Object e : exclude){
                        String key = getKeyFormat(e);
                        if (key.equals(stringEventBusEntry.getKey()) == true){
                            call = false;
                            break;
                        }
                    }
                }

                if (call == true && eventBus != null) {
                    eventBus.post(event);
                }
            }
        }
    }

    public void post(@NonNull Object object, @NonNull Event event) {
        synchronized (mLock) {
            if (mMap.isEmpty()) {
                return;
            }

            org.greenrobot.eventbus.EventBus eventBus = mMap.get(getKeyFormat(object));
            if (eventBus != null) {
                eventBus.post(event);
            }
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("MalformedFormatString")
    @NonNull
    private static String getKeyFormat(@NonNull Object object) {
        return String.format(Locale.US, KEY_FORMAT, object.hashCode());
    }

    private void registerInternal(@NonNull Object object) {
        org.greenrobot.eventbus.EventBus bus = new org.greenrobot.eventbus.EventBus();
        bus.register(object);
        mMap.put(getKeyFormat(object), bus);
    }

    private void unRegisterInternal(@NonNull Object object) {
        String key = getKeyFormat(object);
        org.greenrobot.eventbus.EventBus bus = mMap.get(key);
        if (bus != null) {
            bus.unregister(object);
            mMap.remove(key);
        }
    }
}
