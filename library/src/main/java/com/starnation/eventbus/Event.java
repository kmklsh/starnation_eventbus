package com.starnation.eventbus;

import android.os.Bundle;
import android.support.annotation.NonNull;

/*
 * @author lsh
 * @since 15. 3. 31.
*/
public class Event<EventKey> {

    //======================================================================
    // Constants
    //======================================================================

    private final static Bundle EMPTY_OBJECT = new Bundle();

    //======================================================================
    // Variables
    //======================================================================

    private Bundle mEvent = EMPTY_OBJECT;

    private EventKey mEventKey;

    //======================================================================
    // Constructor
    //======================================================================

    public Event(Bundle event) {
        this(null, event);
    }

    public Event(EventKey eventKey, Bundle event) {
        if (event != null) {
            mEvent = event;
        }
        mEventKey = eventKey;
    }

    public Event(EventKey eventKey) {
        this(eventKey, null);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @NonNull
    public Bundle getEvent() {
        return mEvent;
    }

    public EventKey getEventKey() {
        return mEventKey;
    }
}
