package com.starnation.eventbus;

import org.greenrobot.eventbus.Subscribe;

/*
 * @author lsh
 * @since 2015. 12. 8.
*/
public interface OnEventBusListener<Event> {

    @Subscribe
    void onEvent(Event event);
}
